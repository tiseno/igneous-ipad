//
//  UIImageViewWithAnimation.h
//  ScrollViewMain
//
//  Created by Tiseno on 11/15/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface UIImageViewWithAnimation : UIImageView {
    int screenWidth;
    int screenHeight;
    int xOffSet;
    int yOffSet;
    int paintingX;
    int paintingY;
    int paintingWidth;
    int paintingHeight;
    NSString *nextAnimatedImageName;
}
@property (nonatomic) int screenWidth, screenHeight, paintingX, paintingY, paintingWidth, paintingHeight;
@property (nonatomic, retain) NSString *nextAnimatedImageName;
@property (nonatomic) int xOffSet, yOffSet;
@end
