//
//  testRotateViewController.m
//  testRotate
//
//  Created by Tiseno on 11/17/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "testRotateViewController.h"

@implementation testRotateViewController
@synthesize landscape, portraite;
- (void)dealloc
{
    [landscape release];
    [portraite release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    PortraiteViewController *tPortraitViewController =[[PortraiteViewController alloc] initWithNibName:@"PortraiteViewController" bundle:nil];
    self.portraite = tPortraitViewController;
    [tPortraitViewController release];
    [self.view addSubview:self.portraite.view];
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    /*for(UIView *subview in self.view.subviews)
    {
        [subview removeFromSuperview];
    }*/
    if(toInterfaceOrientation==UIInterfaceOrientationLandscapeLeft || toInterfaceOrientation==UIInterfaceOrientationLandscapeRight)
    {
        LandscapeViewController *tLandScapeViewController =[[LandscapeViewController alloc] initWithNibName:@"LandscapeViewController" bundle:nil];
        self.landscape=tLandScapeViewController;
        [self.portraite.view removeFromSuperview];
        [tLandScapeViewController release];
        [self.view addSubview:self.landscape.view];
    }
    else if(toInterfaceOrientation==UIInterfaceOrientationPortrait || toInterfaceOrientation==UIInterfaceOrientationPortraitUpsideDown)
    {
        PortraiteViewController *tPortraitViewController =[[PortraiteViewController alloc] initWithNibName:@"PortraiteViewController" bundle:nil];
        self.portraite = tPortraitViewController;
        [self.landscape.view removeFromSuperview];
        [tPortraitViewController release];
        [self.view addSubview:self.portraite.view];
    }
    
        
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES;
}

@end
