//
//  testRotateViewController.h
//  testRotate
//
//  Created by Tiseno on 11/17/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LandscapeViewController.h"
#import "PortraiteViewController.h"

@interface testRotateViewController : UIViewController {
    PortraiteViewController *portraite;
    LandscapeViewController *landscape;
}
@property (nonatomic, retain) PortraiteViewController *portraite;
@property (nonatomic, retain) LandscapeViewController *landscape;
@end
