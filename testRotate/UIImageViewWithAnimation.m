//
//  UIImageViewWithAnimation.m
//  ScrollViewMain
//
//  Created by Tiseno on 11/15/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "UIImageViewWithAnimation.h"


@implementation UIImageViewWithAnimation
@synthesize screenWidth, screenHeight, nextAnimatedImageName, xOffSet, yOffSet, paintingX, paintingY, paintingWidth, paintingHeight;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)dealloc
{
    [nextAnimatedImageName release];
    [super dealloc];
}
-(void)imageAnimation
{
    self.frame = CGRectMake((screenWidth-xOffSet), (screenHeight - yOffSet), xOffSet, yOffSet);
    self.image = [UIImage imageNamed:nextAnimatedImageName];
    [UIView beginAnimations:@"ImageFadeIn" context:nil];
    [UIView setAnimationDuration:0.7];
    self.alpha=1.0f;
    [UIView commitAnimations];
}
-(void)backGroundAnimation
{
    self.frame = CGRectMake(xOffSet, yOffSet, 768, 1024);
    self.image = [UIImage imageNamed:nextAnimatedImageName];
    [UIView beginAnimations:@"ImageFadeIn" context:nil];
    [UIView setAnimationDuration:2.0];
    self.alpha=1.0f;
    [UIView commitAnimations];
}
-(void)paintingAnimation
{

    self.frame = CGRectMake(paintingX, paintingY, paintingWidth, paintingHeight);
    self.image = [UIImage imageNamed:nextAnimatedImageName];
    [UIView beginAnimations:@"ImageFadeIn" context:nil];
    [UIView setAnimationDuration:1.0];
    self.alpha=1.0f;
    [UIView commitAnimations];
}
@end
