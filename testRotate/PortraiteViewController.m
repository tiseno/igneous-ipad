//
//  PortraiteViewController.m
//  testRotate
//
//  Created by Tiseno on 11/17/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "PortraiteViewController.h"


@implementation PortraiteViewController
@synthesize mainUIArray;
@synthesize mainScrollView;
@synthesize viewArray;
@synthesize screenSaverImageArray, menuButton, scrollView;
@synthesize clockLabel, dateLabel;
@synthesize dateTimer, clockTimer, animeTimer;
@synthesize timeAndClockBackground, decoPotAnimatingBackground, madeLightAnimatingBackground, artCollectionAnimatingBackground, cravedStrongAnimatingBackground, currentlyInUse;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        animationOffset = -50;
    }
    return self;
}
-(void)updateTimer
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init ];

    [formatter setDateFormat:@"hh:mm:ss"];
    [formatter setTimeStyle:NSDateFormatterLongStyle];
    clockLabel.text = [formatter stringFromDate:[NSDate date]];
    [formatter release];
}
-(void)updateDate
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init ];
    [formatter setDateFormat:@"MM d, YYYY"];
    [formatter setDateStyle:NSDateFormatterLongStyle];
    dateLabel.text = [formatter stringFromDate:[NSDate date]];
    [formatter release];

}
- (void)dealloc
{
    [super dealloc];
    [mainUIArray release];
    [viewArray release]; 
    [mainScrollView release];
    [screenSaverImageArray release];
    [menuButton release];
    [scrollView release];
    [clockLabel release];
    [dateLabel release];
    [timeAndClockBackground release];
    [decoPotAnimatingBackground release];
    [madeLightAnimatingBackground release];
    [artCollectionAnimatingBackground release];
    [cravedStrongAnimatingBackground release];
    [currentlyInUse release];
    [clockTimer release];
    [dateTimer release];
    [animeTimer release];
    
}
-(void)addToViewArray:(UIView*) view
{
    if(self.viewArray==nil)
    {
        NSMutableArray *tviewArray = [[NSMutableArray alloc] init];
        self.viewArray=tviewArray;
        [tviewArray release];
    }
    [viewArray addObject:view];
    
}

-(void)buttonAnime:(UIScrollView*) myScroll
{
    if(!isExpanded)
    {
        [UIView beginAnimations:@"scrollExpand" context:myScroll];
        [UIView setAnimationDuration:0.6];
        [myScroll setFrame:CGRectMake(-4, myScroll.frame.origin.y, myScroll.frame.size.width, myScroll.frame.size.height)];
        [timeAndClockBackground setFrame:CGRectMake(timeAndClockBackground.frame.origin.x, 920, timeAndClockBackground.frame.size.width, timeAndClockBackground.frame.size.height)];
        [clockLabel setFrame:CGRectMake(clockLabel.frame.origin.x, 960, clockLabel.frame.size.width, clockLabel.frame.size.height)];
        [dateLabel setFrame:CGRectMake(dateLabel.frame.origin.x, 933, dateLabel.frame.size.width, dateLabel.frame.size.height)];
        [UIView commitAnimations];
        isExpanded = YES;
        [menuButton setImage:[UIImage imageNamed:@"minus_icon.png"] forState:UIControlStateNormal];
    }else
    {
        [UIView beginAnimations:@"scrollContract" context:myScroll];
        [UIView setAnimationDuration:0.6];
        [myScroll setFrame:CGRectMake(-612, myScroll.frame.origin.y, myScroll.frame.size.width, myScroll.frame.size.height)];
        [clockLabel setFrame:CGRectMake(clockLabel.frame.origin.x, 1076, clockLabel.frame.size.width, clockLabel.frame.size.height)];
        [dateLabel setFrame:CGRectMake(dateLabel.frame.origin.x, 1076, dateLabel.frame.size.width, dateLabel.frame.size.height)];
        [timeAndClockBackground setFrame:CGRectMake(timeAndClockBackground.frame.origin.x, 1076, timeAndClockBackground.frame.size.width, timeAndClockBackground.frame.size.height)];
        [UIView commitAnimations];
        isExpanded = NO;
        [menuButton setImage:[UIImage imageNamed:@"plus_icon.png"] forState:UIControlStateNormal];
    }
}
-(void)initialize_introView_1:(int)width
{
    UIView *introView_1=[[UIView alloc] initWithFrame: CGRectMake(width, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [introView_1 setBackgroundColor:[UIColor colorWithRed:0.27 green:0.27 blue:0.27 alpha:1.0]];
    UIImageView *backgroundImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"intro_1.png"]];
    backgroundImage.frame=CGRectMake(width, 0, self.view.frame.size.width, self.view.frame.size.height);
    
    [introView_1 addSubview:backgroundImage];
    [self addToViewArray:introView_1];
    
    [backgroundImage release];
    [introView_1 release];
}
-(void)initialize_introView_2:(int)width
{
    UIView *introView_2=[[UIView alloc] initWithFrame: CGRectMake(width, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [introView_2 setBackgroundColor:[UIColor colorWithRed:0.27 green:0.27 blue:0.27 alpha:1.0]];
    UIImageView *backgroundImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"intro_2.png"]];
    
    [introView_2 addSubview:backgroundImage];
    [self addToViewArray:introView_2];
    
    [backgroundImage release];
    [introView_2 release];
}
-(void)initialize_Decor_Pots_1:(int)width
{
    UIView *decor_Pots_1=[[UIView alloc] initWithFrame: CGRectMake(width, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [decor_Pots_1 setBackgroundColor:[UIColor colorWithRed:0.27 green:0.27 blue:0.27 alpha:1.0]];
    UIImageView *backgroundImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"deco_pots_1.png"]];
    backgroundImage.frame = CGRectMake(0, 0, 868, 1024);
    decoPotAnimatingBackground = backgroundImage;
    
    [decor_Pots_1 addSubview:decoPotAnimatingBackground];
    [self addToViewArray:decor_Pots_1];
    
    [backgroundImage release];
    [decor_Pots_1 release];
}
-(void)initialize_Decor_Pots_2:(int)width
{
    UIView *decor_Pots_2=[[UIView alloc] initWithFrame: CGRectMake(width, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [decor_Pots_2 setBackgroundColor:[UIColor colorWithRed:0.27 green:0.27 blue:0.27 alpha:1.0]];
    UIImageViewWithAnimation *backgroundImage = [[UIImageViewWithAnimation alloc] initWithImage:[UIImage imageNamed:@"deco_pot_2.png"]];
    
    UIImage *leftButtonImage = [UIImage imageNamed:@"deco_thumbnail_1.png"];
    UIButton *leftDecorButton = [UIButton buttonWithType:UIButtonTypeCustom];
    leftDecorButton.frame = CGRectMake(575, 265, 81, 81);
    [leftDecorButton setBackgroundImage:leftButtonImage forState:UIControlStateNormal];
    [leftDecorButton addTarget:self action:@selector(deco_1_LeftButton:)
              forControlEvents:UIControlEventTouchDown];
    
    UIImage *rightButtonImage = [UIImage imageNamed:@"deco_thumbnail_2.png"];
    UIButton *rightDecorButton = [UIButton buttonWithType:UIButtonTypeCustom];
    rightDecorButton.frame = CGRectMake(661, 265, 81, 81);
    [rightDecorButton setBackgroundImage:rightButtonImage forState:UIControlStateNormal];
    [rightDecorButton addTarget:self action:@selector(deco_1_RightButton:)
               forControlEvents:UIControlEventTouchDown];
    
    [decor_Pots_2 addSubview:backgroundImage];
    [decor_Pots_2 addSubview:leftDecorButton];
    [decor_Pots_2 addSubview:rightDecorButton];
    [self addToViewArray:decor_Pots_2];
    
    [backgroundImage release];
    [decor_Pots_2 release];
}
-(void)initialize_Decor_Pots_3:(int)width
{
    UIView *decor_Pots_3=[[UIView alloc] initWithFrame: CGRectMake(width, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [decor_Pots_3 setBackgroundColor:[UIColor colorWithRed:0.27 green:0.27 blue:0.27 alpha:1.0]];
    UIImageView *backgroundImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"living_room.png"]];
    
    UIImageViewWithAnimation *potImage = [[UIImageViewWithAnimation alloc] initWithImage:[UIImage imageNamed:@"pot_3"]];
    potImage.frame = CGRectMake(width+(self.view.frame.size.width-154), width+(self.view.frame.size.height - 560), 154, 560);
    
    UIImage *leftButtonImage = [UIImage imageNamed:@"deco_thumbnail_3.png"];
    UIButton *leftDecorButton = [UIButton buttonWithType:UIButtonTypeCustom];
    leftDecorButton.frame = CGRectMake(576, 917, 82, 82);
    [leftDecorButton setBackgroundImage:leftButtonImage forState:UIControlStateNormal];
    [leftDecorButton addTarget:self action:@selector(deco_2_LeftButton:)
              forControlEvents:UIControlEventTouchDown];
    
    UIImage *rightButtonImage = [UIImage imageNamed:@"deco_thumbnail_4.png"];
    UIButton *rightDecorButton = [UIButton buttonWithType:UIButtonTypeCustom];
    rightDecorButton.frame = CGRectMake(660, 917, 82, 82);
    [rightDecorButton setBackgroundImage:rightButtonImage forState:UIControlStateNormal];
    [rightDecorButton addTarget:self action:@selector(deco_2_RightButton:)
               forControlEvents:UIControlEventTouchDown];
    
    [decor_Pots_3 addSubview:backgroundImage];
    [decor_Pots_3 addSubview:potImage];
    [decor_Pots_3 addSubview:leftDecorButton];
    [decor_Pots_3 addSubview:rightDecorButton];
    
    [self addToViewArray:decor_Pots_3];
    
    [decor_Pots_3 release];
    [backgroundImage release];
    [potImage release];
}
-(void)initialize_Craved_Stone_1:(int)width
{
    UIView *craved_Stone_1=[[UIView alloc] initWithFrame: CGRectMake(width, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [craved_Stone_1 setBackgroundColor:[UIColor colorWithRed:0.27 green:0.27 blue:0.27 alpha:1.0]];
    UIImageView *backgroundImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"carved_stone.png"]];
    backgroundImage.frame = CGRectMake(0, 0, 868, 1024);
    cravedStrongAnimatingBackground = backgroundImage;
    
    [craved_Stone_1 addSubview:cravedStrongAnimatingBackground];
    [self addToViewArray:craved_Stone_1];
    
    [backgroundImage release];
    [craved_Stone_1 release];
}
-(void)initialize_Craved_Stone_2:(int)width
{
    UIView *craved_Stone_2=[[UIView alloc] initWithFrame: CGRectMake(width, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [craved_Stone_2 setBackgroundColor:[UIColor colorWithRed:0.27 green:0.27 blue:0.27 alpha:1.0]];
    UIImageViewWithAnimation *backgroundImage = [[UIImageViewWithAnimation alloc] initWithImage:[UIImage imageNamed:@"carved_stone_1.png"]];
    
    UIImage *leftButtonImage = [UIImage imageNamed:@"carved_thumbnail_1.png"];
    UIButton *leftCravedButton = [UIButton buttonWithType:UIButtonTypeCustom];
    leftCravedButton.frame = CGRectMake(482, 268, 82, 82);
    [leftCravedButton setBackgroundImage:leftButtonImage forState:UIControlStateNormal];
    [leftCravedButton addTarget:self action:@selector(craved_2_LeftButton:)
               forControlEvents:UIControlEventTouchDown];
    
    UIImage *middleButtonImage = [UIImage imageNamed:@"carved_thumbnail_2.png"];
    UIButton *middleCravedButton = [UIButton buttonWithType:UIButtonTypeCustom];
    middleCravedButton.frame = CGRectMake(566, 268, 82, 82);
    [middleCravedButton setBackgroundImage:middleButtonImage forState:UIControlStateNormal];
    [middleCravedButton addTarget:self action:@selector(craved_2_MiddleButton:)
                 forControlEvents:UIControlEventTouchDown];
    
    UIImage *rightButtonImage = [UIImage imageNamed:@"carved_thumbnail_3.png"];
    UIButton *rightCravedButton = [UIButton buttonWithType:UIButtonTypeCustom];
    rightCravedButton.frame = CGRectMake(651, 268, 82, 82);
    [rightCravedButton setBackgroundImage:rightButtonImage forState:UIControlStateNormal];
    [rightCravedButton addTarget:self action:@selector(craved_2_RightButton:)
                forControlEvents:UIControlEventTouchDown];
    
    [craved_Stone_2 addSubview:backgroundImage];
    [craved_Stone_2 addSubview:leftCravedButton];
    [craved_Stone_2 addSubview:middleCravedButton];
    [craved_Stone_2 addSubview:rightCravedButton];
    [self addToViewArray:craved_Stone_2];
    
    [backgroundImage release];
    [craved_Stone_2 release];
}
-(void)initialize_Craved_Stone_3:(int)width
{
    UIView *craved_Stone_3=[[UIView alloc] initWithFrame: CGRectMake(width, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [craved_Stone_3 setBackgroundColor:[UIColor colorWithRed:0.27 green:0.27 blue:0.27 alpha:1.0]];
    UIImageView *backgroundImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"carved_stone_4.png"]];
    
    [craved_Stone_3 addSubview:backgroundImage];
    
    [self addToViewArray:craved_Stone_3];
    
    [backgroundImage release];
    [craved_Stone_3 release];
}
-(void)initialize_Craved_Stone_4:(int)width
{
    UIView *craved_Stone_4=[[UIView alloc] initWithFrame: CGRectMake(width, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [craved_Stone_4 setBackgroundColor:[UIColor colorWithRed:0.27 green:0.27 blue:0.27 alpha:1.0]];
    UIImageView *backgroundImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"carved_stone_5.png"]];
    
    [craved_Stone_4 addSubview:backgroundImage];
    
    [self addToViewArray:craved_Stone_4];
    
    [backgroundImage release];
    [craved_Stone_4 release];
}
-(void)initialize_Art_Collection_1:(int)width
{
    UIView *art_coll_1=[[UIView alloc] initWithFrame: CGRectMake(width, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [art_coll_1 setBackgroundColor:[UIColor colorWithRed:0.27 green:0.27 blue:0.27 alpha:1.0]];
    UIImageView *backgroundImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"art_collection.png"]];
    backgroundImage.frame = CGRectMake(0, 0, 868, 1024);
    artCollectionAnimatingBackground = backgroundImage;
    
    [art_coll_1 addSubview:artCollectionAnimatingBackground];
    
    [self addToViewArray:art_coll_1];
    
    [backgroundImage release];
    [art_coll_1 release];
}
-(void)initialize_Art_Collection_2:(int)width
{
    UIView *art_coll_2=[[UIView alloc] initWithFrame: CGRectMake(width, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [art_coll_2 setBackgroundColor:[UIColor colorWithRed:0.27 green:0.27 blue:0.27 alpha:1.0]];
    UIImageViewWithAnimation *backgroundImage = [[UIImageViewWithAnimation alloc] initWithImage:[UIImage imageNamed:@"art_collection_1.png"]];
    
    UIImage *leftButtonImage = [UIImage imageNamed:@"art_thumbnail_1.png"];
    UIButton *leftArtButton = [UIButton buttonWithType:UIButtonTypeCustom];
    leftArtButton.frame = CGRectMake(566, 268, 82, 82);
    [leftArtButton setBackgroundImage:leftButtonImage forState:UIControlStateNormal];
    [leftArtButton addTarget:self action:@selector(art_2_LeftButton:)
            forControlEvents:UIControlEventTouchDown];
    
    UIImage *rightButtonImage = [UIImage imageNamed:@"art_thumbnail_2.png"];
    UIButton *rightArtButton = [UIButton buttonWithType:UIButtonTypeCustom];
    rightArtButton.frame = CGRectMake(651, 268, 82, 82);
    [rightArtButton setBackgroundImage:rightButtonImage forState:UIControlStateNormal];
    [rightArtButton addTarget:self action:@selector(art_2_RightButton:)
             forControlEvents:UIControlEventTouchDown];
    
    [art_coll_2 addSubview:backgroundImage];
    [art_coll_2 addSubview:leftArtButton];
    [art_coll_2 addSubview:rightArtButton];
    [self addToViewArray:art_coll_2];
    
    [backgroundImage release];
    [art_coll_2 release];
}
-(void)initialize_Art_Collection_3:(int)width
{
    UIView *art_coll_3=[[UIView alloc] initWithFrame: CGRectMake(width, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [art_coll_3 setBackgroundColor:[UIColor colorWithRed:0.27 green:0.27 blue:0.27 alpha:1.0]];
    UIImageView *backgroundImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"diningroom.png"]];
    
    UIImageViewWithAnimation *paintingImage = [[UIImageViewWithAnimation alloc] initWithImage:[UIImage imageNamed:@"art_collection_3.png"]];
    paintingImage.frame = CGRectMake(width+117, 271, 532, 506);
    paintingImage.screenWidth = width;
    
    UIImage *leftButtonImage = [UIImage imageNamed:@"art_thumbnail_3.png"];
    UIButton *leftArtButton = [UIButton buttonWithType:UIButtonTypeCustom];
    leftArtButton.frame = CGRectMake(576, 922, 82, 82);
    [leftArtButton setBackgroundImage:leftButtonImage forState:UIControlStateNormal];
    [leftArtButton addTarget:self action:@selector(art_3_LeftButton:)
            forControlEvents:UIControlEventTouchDown];
    
    UIImage *rightButtonImage = [UIImage imageNamed:@"art_thumbnail_4.png"];
    UIButton *rightArtButton = [UIButton buttonWithType:UIButtonTypeCustom];
    rightArtButton.frame = CGRectMake(660, 922, 82, 82);
    [rightArtButton setBackgroundImage:rightButtonImage forState:UIControlStateNormal];
    [rightArtButton addTarget:self action:@selector(art_3_RightButton:)
             forControlEvents:UIControlEventTouchDown];
    
    [art_coll_3 addSubview:backgroundImage];
    [art_coll_3 addSubview:paintingImage];
    [art_coll_3 addSubview:leftArtButton];
    [art_coll_3 addSubview:rightArtButton];
    [self addToViewArray:art_coll_3];
    
    [backgroundImage release];
    [art_coll_3 release];
    [paintingImage release];
}
-(void)initialize_Art_Collection_4:(int)width
{
    UIView *art_coll_4=[[UIView alloc] initWithFrame: CGRectMake(width, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [art_coll_4 setBackgroundColor:[UIColor colorWithRed:0.27 green:0.27 blue:0.27 alpha:1.0]];
    UIImageView *backgroundImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"art_collection_5.png"]];
    
    [art_coll_4 addSubview:backgroundImage];
    [self addToViewArray:art_coll_4];
    
    [backgroundImage release];
    [art_coll_4 release];
}
-(void)initialize_Made_Light_1:(int)width
{
    UIView *made_Light_1=[[UIView alloc] initWithFrame: CGRectMake(width, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [made_Light_1 setBackgroundColor:[UIColor colorWithRed:0.27 green:0.27 blue:0.27 alpha:1.0]];
    UIImageView *backgroundImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"made_light.png"]];
    backgroundImage.frame = CGRectMake(0, 0, 868, 1024);
    madeLightAnimatingBackground = backgroundImage;
    
    [made_Light_1 addSubview:madeLightAnimatingBackground];
    [self addToViewArray:made_Light_1];
    
    [backgroundImage release];
    [made_Light_1 release];
}
-(void)initialize_Made_Light_2:(int)width
{
    UIView *made_Light_2=[[UIView alloc] initWithFrame: CGRectMake(width, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [made_Light_2 setBackgroundColor:[UIColor colorWithRed:0.27 green:0.27 blue:0.27 alpha:1.0]];
    UIImageViewWithAnimation *backgroundImage = [[UIImageViewWithAnimation alloc] initWithImage:[UIImage imageNamed:@"made_light_1.png"]];
    
    UIImage *leftButtonImage = [UIImage imageNamed:@"made_thumbnail_1.png"];
    UIButton *leftMadetButton = [UIButton buttonWithType:UIButtonTypeCustom];
    leftMadetButton.frame = CGRectMake(568, 270, 82, 82);
    [leftMadetButton setBackgroundImage:leftButtonImage forState:UIControlStateNormal];
    [leftMadetButton addTarget:self action:@selector(made_3_LeftButton:)
              forControlEvents:UIControlEventTouchDown];
    
    UIImage *rightButtonImage = [UIImage imageNamed:@"made_thumbnail_2.png"];
    UIButton *rightMadeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    rightMadeButton.frame = CGRectMake(649, 270, 82, 82);
    [rightMadeButton setBackgroundImage:rightButtonImage forState:UIControlStateNormal];
    [rightMadeButton addTarget:self action:@selector(made_3_RightButton:)
              forControlEvents:UIControlEventTouchDown];
    
    [made_Light_2 addSubview:backgroundImage];
    [made_Light_2 addSubview:leftMadetButton];
    [made_Light_2 addSubview:rightMadeButton];
    [self addToViewArray:made_Light_2];
    
    [backgroundImage release];
    [made_Light_2 release];
}
-(void)initialize_Made_Light_3:(int)width
{
    UIView *made_Light_3=[[UIView alloc] initWithFrame: CGRectMake(width, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [made_Light_3 setBackgroundColor:[UIColor colorWithRed:0.27 green:0.27 blue:0.27 alpha:1.0]];
    UIImageView *backgroundImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"made_light_3.png"]];
    
    UIImage *touchButtonImage = [UIImage imageNamed:@"made_touch.png"];
    UIButton *touchButton = [UIButton buttonWithType:UIButtonTypeCustom];
    touchButton.frame = CGRectMake(293, 443, 163, 41);
    [touchButton setBackgroundImage:touchButtonImage forState:UIControlStateNormal];
    [touchButton addTarget:self action:@selector(made_3_TouchButton:)
          forControlEvents:UIControlEventTouchDown];
    
    
    [made_Light_3 addSubview:backgroundImage];
    [made_Light_3 addSubview:touchButton];
    [self addToViewArray:made_Light_3];
    
    [backgroundImage release];
    [made_Light_3 release];
}
-(void)initialize_Made_Light_4:(int)width
{
    UIView *made_Light_4=[[UIView alloc] initWithFrame: CGRectMake(width, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [made_Light_4 setBackgroundColor:[UIColor colorWithRed:0.27 green:0.27 blue:0.27 alpha:1.0]];
    UIImageView *backgroundImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"made_light_5.png"]];
    //backgroundImage.frame=CGRectMake(width+0, 0, self.view.frame.size.width, self.view.frame.size.height);
    
    [made_Light_4 addSubview:backgroundImage];
    [self addToViewArray:made_Light_4];
    
    [backgroundImage release];
    [made_Light_4 release];
}
-(void)initialize_Contact:(int)width
{
    UIView *contact=[[UIView alloc] initWithFrame: CGRectMake(width, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [contact setBackgroundColor:[UIColor colorWithRed:0.27 green:0.27 blue:0.27 alpha:1.0]];
    UIImageView *backgroundImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"contact.png"]];
    
    
    UIImage *websiteButtonImage = [UIImage imageNamed:@"btn_link.png"];
    UIButton *websiteButton = [UIButton buttonWithType:UIButtonTypeCustom];
    websiteButton.frame = CGRectMake(115, 313, 342, 37);
    [websiteButton setBackgroundImage:websiteButtonImage forState:UIControlStateNormal];
    [websiteButton addTarget:self action:@selector(website_Button:)
              forControlEvents:UIControlEventTouchDown];
    
    [contact addSubview:backgroundImage];
    [contact addSubview:websiteButton];
    [self addToViewArray:contact];
    
    [backgroundImage release];
    [contact release];
}
-(void)backgroundReAnimation
{
    currentlyInUse.frame = CGRectMake(currentlyInUse.frame.origin.x, currentlyInUse.frame.origin.y, currentlyInUse.frame.size.width, currentlyInUse.frame.size.height);
    if(currentlyInUse.frame.origin.x != 0)
    {
        
        [UIView beginAnimations:@"decoPotAnim1" context:nil];
        [UIView setAnimationDuration:3.0];
        currentlyInUse.frame = CGRectMake(currentlyInUse.frame.origin.x + animationOffset, currentlyInUse.frame.origin.y, currentlyInUse.frame.size.width, currentlyInUse.frame.size.height);
        [UIView commitAnimations];
        animationOffset = -animationOffset;
    }else
    {
        [UIView beginAnimations:@"decoPotAnim1" context:nil];
        [UIView setAnimationDuration:3.0];
        currentlyInUse.frame = CGRectMake(currentlyInUse.frame.origin.x + animationOffset, currentlyInUse.frame.origin.y, currentlyInUse.frame.size.width, currentlyInUse.frame.size.height);
        //[UIView setAnimationDidStopSelector:
        [UIView commitAnimations];
        animationOffset = -animationOffset;
    }
}
-(IBAction)menuButtonTapped
{
    [self buttonAnime:scrollView];
}
-(IBAction)buttonOneTapped
{
    currentPage = mainScrollView.contentOffset.x/768;
    if(currentPage !=2)
    {
    animationOffset = -50;
    [animeTimer invalidate];
    currentlyInUse = decoPotAnimatingBackground;
    CGRect jumpedToPage = CGRectMake(2*widthOffset, 0, self.mainScrollView.frame.size.width, self.mainScrollView.frame.size.height);
    [mainScrollView scrollRectToVisible:jumpedToPage animated:YES];
    animeTimer = [NSTimer scheduledTimerWithTimeInterval:3.0 target:self selector:@selector(backgroundReAnimation) userInfo:nil repeats:YES];
    }
}
-(IBAction)buttonTwoTapped
{
    currentPage = mainScrollView.contentOffset.x/768;
    if(currentPage !=5)
    {
    animationOffset = -50;
    [animeTimer invalidate];
    currentlyInUse = cravedStrongAnimatingBackground;
    CGRect jumpedToPage = CGRectMake(5*widthOffset, 0, self.mainScrollView.frame.size.width, self.mainScrollView.frame.size.height);
    [mainScrollView scrollRectToVisible:jumpedToPage animated:YES];
    animeTimer = [NSTimer scheduledTimerWithTimeInterval:3.0 target:self selector:@selector(backgroundReAnimation) userInfo:nil repeats:YES];
    }
}
-(IBAction)buttonThreeTapped
{
    currentPage = mainScrollView.contentOffset.x/768;
    if(currentPage !=9)
    {
    animationOffset = -50;
    [animeTimer invalidate];
    currentlyInUse = artCollectionAnimatingBackground;
    CGRect jumpedToPage = CGRectMake(9*widthOffset, 0, self.mainScrollView.frame.size.width, self.mainScrollView.frame.size.height);
    [mainScrollView scrollRectToVisible:jumpedToPage animated:YES];
    animeTimer = [NSTimer scheduledTimerWithTimeInterval:3.0 target:self selector:@selector(backgroundReAnimation) userInfo:nil repeats:YES];
    }
}
-(IBAction)buttonFourTapped
{
    currentPage = mainScrollView.contentOffset.x/768;
    if(currentPage !=13)
    {
    animationOffset = -50;
    [animeTimer invalidate];
    currentlyInUse = madeLightAnimatingBackground;
    CGRect jumpedToPage = CGRectMake(13*widthOffset, 0, self.mainScrollView.frame.size.width, self.mainScrollView.frame.size.height);
    [mainScrollView scrollRectToVisible:jumpedToPage animated:YES];
    animeTimer = [NSTimer scheduledTimerWithTimeInterval:3.0 target:self selector:@selector(backgroundReAnimation) userInfo:nil repeats:YES];
    }
}
-(IBAction)buttonFiveTapped
{
    [[UIApplication sharedApplication]  openURL:[NSURL URLWithString:@"http://www.facebook.com/pages/The-Igneous/242564919096890?sk=info"]];
    
}

-(IBAction)deco_1_RightButton:(id)sender
{
    UIView *currentView = [sender superview];
    UIImageView *subView = [[currentView subviews] objectAtIndex:0];
    [UIView beginAnimations:@"bgFadeOut" context:nil];
    [UIView setAnimationDuration:0.3];
    [UIView setAnimationDelegate:subView];
    ((UIImageViewWithAnimation*)subView).screenWidth = self.view.frame.size.width;
    ((UIImageViewWithAnimation*)subView).screenHeight = self.view.frame.size.width;
    ((UIImageViewWithAnimation*)subView).nextAnimatedImageName=@"deco_pot_3.png";
    [UIView setAnimationDidStopSelector:@selector(backGroundAnimation)];
    subView.alpha = 0.0f;
    [UIView commitAnimations];
}
-(IBAction)deco_1_LeftButton:(id)sender
{
    UIView *currentView = [sender superview];
    UIImageView *subView = [[currentView subviews] objectAtIndex:0];
    [UIView beginAnimations:@"bgFadeOut" context:nil];
    [UIView setAnimationDuration:0.3];
    [UIView setAnimationDelegate:subView];
    ((UIImageViewWithAnimation*)subView).screenWidth = self.view.frame.size.width;
    ((UIImageViewWithAnimation*)subView).screenHeight = self.view.frame.size.width;
    ((UIImageViewWithAnimation*)subView).nextAnimatedImageName=@"deco_pot_2.png";
    [UIView setAnimationDidStopSelector:@selector(backGroundAnimation)];
    subView.alpha = 0.0f;
    [UIView commitAnimations];
}
-(IBAction)deco_2_RightButton:(id)sender
{
    UIView *currentView = [sender superview];
    UIImageView *subView = [[currentView subviews]objectAtIndex:1];
    [UIView beginAnimations:@"potFadeOut" context:nil];
    [UIView setAnimationDuration:0.2];
    [UIView setAnimationDelegate:subView];
    ((UIImageViewWithAnimation*)subView).screenWidth=self.view.frame.size.width;
    ((UIImageViewWithAnimation*)subView).screenHeight=self.view.frame.size.height;
    ((UIImageViewWithAnimation*)subView).nextAnimatedImageName= @"pot_4.png";
    ((UIImageViewWithAnimation*)subView).xOffSet = 188;
    ((UIImageViewWithAnimation*)subView).yOffSet = 410;
    [UIView setAnimationDidStopSelector:@selector(imageAnimation)];
    subView.alpha=0.0f;
    [UIView commitAnimations];   
}
-(IBAction)deco_2_LeftButton:(id)sender
{
    UIView *currentView = [sender superview];
    UIImageView *subView = [[currentView subviews]objectAtIndex:1];
    [UIView beginAnimations:@"potFadeOut" context:nil];
    [UIView setAnimationDuration:0.2];
    [UIView setAnimationDelegate:subView];
    ((UIImageViewWithAnimation*)subView).screenWidth=self.view.frame.size.width;
    ((UIImageViewWithAnimation*)subView).screenHeight=self.view.frame.size.height;
    ((UIImageViewWithAnimation*)subView).nextAnimatedImageName= @"pot_3.png";
    ((UIImageViewWithAnimation*)subView).xOffSet = 154;
    ((UIImageViewWithAnimation*)subView).yOffSet = 580;
    [UIView setAnimationDidStopSelector:@selector(imageAnimation)];
    subView.alpha=0.0f;
    [UIView commitAnimations];
}

-(IBAction)craved_2_LeftButton:(id)sender
{
    UIView *currentView = [sender superview];
    UIImageView *subView = [[currentView subviews] objectAtIndex:0];
    [UIView beginAnimations:@"bgFadeOut" context:nil];
    [UIView setAnimationDuration:0.2];
    [UIView setAnimationDelegate:subView];
    ((UIImageViewWithAnimation*)subView).screenWidth = self.view.frame.size.width;
    ((UIImageViewWithAnimation*)subView).screenHeight = self.view.frame.size.width;
    ((UIImageViewWithAnimation*)subView).nextAnimatedImageName=@"carved_stone_1";
    [UIView setAnimationDidStopSelector:@selector(backGroundAnimation)];
    subView.alpha = 0.0f;
    [UIView commitAnimations];
}
-(IBAction)craved_2_MiddleButton:(id)sender
{
    UIView *currentView = [sender superview];
    UIImageView *subView = [[currentView subviews] objectAtIndex:0];
    [UIView beginAnimations:@"bgFadeOut" context:nil];
    [UIView setAnimationDuration:0.2];
    [UIView setAnimationDelegate:subView];
    ((UIImageViewWithAnimation*)subView).screenWidth = self.view.frame.size.width;
    ((UIImageViewWithAnimation*)subView).screenHeight = self.view.frame.size.width;
    ((UIImageViewWithAnimation*)subView).nextAnimatedImageName=@"carved_stone_2";
    [UIView setAnimationDidStopSelector:@selector(backGroundAnimation)];
    subView.alpha = 0.0f;
    [UIView commitAnimations];
}
-(IBAction)craved_2_RightButton:(id)sender
{
    UIView *currentView = [sender superview];
    UIImageView *subView = [[currentView subviews] objectAtIndex:0];
    [UIView beginAnimations:@"bgFadeOut" context:nil];
    [UIView setAnimationDuration:0.2];
    [UIView setAnimationDelegate:subView];
    ((UIImageViewWithAnimation*)subView).nextAnimatedImageName=@"carved_stone_3";
    [UIView setAnimationDidStopSelector:@selector(backGroundAnimation)];
    subView.alpha = 0.0f;
    [UIView commitAnimations];
}
-(IBAction)art_2_LeftButton:(id)sender
{
    UIView *currentView = [sender superview];
    UIImageView *subView = [[currentView subviews] objectAtIndex:0];
    [UIView beginAnimations:@"bgFadeOut" context:nil];
    [UIView setAnimationDuration:0.2];
    [UIView setAnimationDelegate:subView];
    ((UIImageViewWithAnimation*)subView).nextAnimatedImageName=@"art_collection_1.png";
    [UIView setAnimationDidStopSelector:@selector(backGroundAnimation)];
    subView.alpha = 0.0f;
    [UIView commitAnimations];
}
-(IBAction)art_2_RightButton:(id)sender
{
    UIView *currentView = [sender superview];
    UIImageView *subView = [[currentView subviews] objectAtIndex:0];
    [UIView beginAnimations:@"bgFadeOut" context:nil];
    [UIView setAnimationDuration:0.2];
    [UIView setAnimationDelegate:subView];
    ((UIImageViewWithAnimation*)subView).nextAnimatedImageName=@"art_collection_2.png";
    [UIView setAnimationDidStopSelector:@selector(backGroundAnimation)];
    subView.alpha = 0.0f;
    [UIView commitAnimations];
}
-(IBAction)art_3_LeftButton:(id)sender
{
    UIView *currentView = [sender superview];
    UIImageView *subView = [[currentView subviews]objectAtIndex:1];
    
    [UIView beginAnimations:@"potFadeOut" context:nil];
    [UIView setAnimationDuration:0.2];
    [UIView setAnimationDelegate:subView];
    ((UIImageViewWithAnimation*)subView).nextAnimatedImageName= @"art_collection_4.png";
    ((UIImageViewWithAnimation*)subView).paintingHeight=391;
    ((UIImageViewWithAnimation*)subView).paintingX=113;
    ((UIImageViewWithAnimation*)subView).paintingY=290;
    ((UIImageViewWithAnimation*)subView).paintingWidth=520;
    [UIView setAnimationDidStopSelector:@selector(paintingAnimation)];
    subView.alpha=0.0f;
    [UIView commitAnimations];
}
-(IBAction)art_3_RightButton:(id)sender
{
    UIView *currentView = [sender superview];
    UIImageView *subView = [[currentView subviews]objectAtIndex:1];
    [UIView beginAnimations:@"potFadeOut" context:nil];
    [UIView setAnimationDuration:0.2];
    [UIView setAnimationDelegate:subView];
    ((UIImageViewWithAnimation*)subView).nextAnimatedImageName= @"art_collection_3.png";
    ((UIImageViewWithAnimation*)subView).paintingX=117;
    ((UIImageViewWithAnimation*)subView).paintingY=271;
    ((UIImageViewWithAnimation*)subView).paintingWidth=532;
    ((UIImageViewWithAnimation*)subView).paintingHeight=506;
    [UIView setAnimationDidStopSelector:@selector(paintingAnimation)];
    subView.alpha=0.0f;
    [UIView commitAnimations];
}
-(IBAction)made_3_LeftButton:(id)sender
{
    UIView *currentView = [sender superview];
    UIImageView *subView = [[currentView subviews] objectAtIndex:0];
    [UIView beginAnimations:@"bgFadeOut" context:nil];
    [UIView setAnimationDuration:0.2];
    [UIView setAnimationDelegate:subView];
    ((UIImageViewWithAnimation*)subView).nextAnimatedImageName=@"made_light_1.png";
    [UIView setAnimationDidStopSelector:@selector(backGroundAnimation)];
    subView.alpha = 0.0f;
    [UIView commitAnimations];
}
-(IBAction)made_3_RightButton:(id)sender
{
    UIView *currentView = [sender superview];
    UIImageView *subView = [[currentView subviews] objectAtIndex:0];
    [UIView beginAnimations:@"bgFadeOut" context:nil];
    [UIView setAnimationDuration:0.2];
    [UIView setAnimationDelegate:subView];
    ((UIImageViewWithAnimation*)subView).nextAnimatedImageName=@"made_light_2.png";
    [UIView setAnimationDidStopSelector:@selector(backGroundAnimation)];
    subView.alpha = 0.0f;
    [UIView commitAnimations];
}
-(IBAction)made_3_TouchButton:(id)sender
{
    UIView *currentView = [sender superview];
    UIImageView *subView = [[currentView subviews] objectAtIndex:0];
    if(!isLightOn)
    {
        subView.image = [UIImage imageNamed:@"made_light_4.png"];
        isLightOn = YES;
    }else
    {
        subView.image = [UIImage imageNamed:@"made_light_3.png"];
        isLightOn = NO;
    }
}
-(IBAction)website_Button:(id)sender
{
    [[UIApplication sharedApplication]  openURL:[NSURL URLWithString:@"http://www.theigneous.com/"]];
}
- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    if(animeTimer!=nil)
    {
        [animeTimer invalidate];
        animeTimer=nil;
    }
}
-(void)scrollViewDidEndDecelerating:(UIScrollView*) scrollView
{
    animationOffset = -50;
    currentPage = mainScrollView.contentOffset.x/768;
    if(currentPage == 2)
    {
        currentlyInUse = decoPotAnimatingBackground;
        animeTimer = [NSTimer scheduledTimerWithTimeInterval:3.0 target:self selector:@selector(backgroundReAnimation) userInfo:nil repeats:YES];
    }else if(currentPage == 5)
    {
        currentlyInUse = cravedStrongAnimatingBackground;
        animeTimer = [NSTimer scheduledTimerWithTimeInterval:3.0 target:self selector:@selector(backgroundReAnimation) userInfo:nil repeats:YES];
    }else if(currentPage == 9)
    {
        currentlyInUse = artCollectionAnimatingBackground;
        animeTimer = [NSTimer scheduledTimerWithTimeInterval:3.0 target:self selector:@selector(backgroundReAnimation) userInfo:nil repeats:YES];
    }else if(currentPage == 13)
    {
        currentlyInUse = madeLightAnimatingBackground;
        animeTimer = [NSTimer scheduledTimerWithTimeInterval:3.0 target:self selector:@selector(backgroundReAnimation) userInfo:nil repeats:YES];
    }/*else
    {
        if(currentlyInUse == decoPotAnimatingBackground)
        {
            currentlyInUse.frame = CGRectMake(0, 0, currentlyInUse.frame.size.width, currentlyInUse.frame.size.height);
        }
        if(currentlyInUse == cravedStrongAnimatingBackground)
        {
            currentlyInUse.frame = CGRectMake(0, 0, currentlyInUse.frame.size.width, currentlyInUse.frame.size.height);
        }
        if(currentlyInUse == artCollectionAnimatingBackground)
        {
            currentlyInUse.frame = CGRectMake(0, 0, currentlyInUse.frame.size.width, currentlyInUse.frame.size.height);
        }
        if(currentlyInUse == madeLightAnimatingBackground)
        {
            currentlyInUse.frame = CGRectMake(0, 0, currentlyInUse.frame.size.width, currentlyInUse.frame.size.height);
        }
    }*/
}
-(void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView
{
    currentlyInUse.frame = CGRectMake(0, 0, currentlyInUse.frame.size.width, currentlyInUse.frame.size.height);
}
#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    isExpanded = NO;
    isLightOn = NO;
    clockTimer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(updateTimer) userInfo:nil repeats:YES];
    dateTimer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(updateDate) userInfo:nil repeats:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
    [self initialize_introView_1:0*widthOffset];
    [self initialize_introView_2:1*widthOffset];
    [self initialize_Decor_Pots_1:2*widthOffset];
    [self initialize_Decor_Pots_2:3*widthOffset];
    [self initialize_Decor_Pots_3:4*widthOffset];
    [self initialize_Craved_Stone_1:5*widthOffset];
    [self initialize_Craved_Stone_2:6*widthOffset];
    [self initialize_Craved_Stone_3:7*widthOffset];
    [self initialize_Craved_Stone_4:8*widthOffset];
    [self initialize_Art_Collection_1:9*widthOffset];
    [self initialize_Art_Collection_2:10*widthOffset];
    [self initialize_Art_Collection_3:11*widthOffset];
    [self initialize_Art_Collection_4:12*widthOffset];
    [self initialize_Made_Light_1:13*widthOffset];
    [self initialize_Made_Light_2:14*widthOffset];
    [self initialize_Made_Light_3:15*widthOffset];
    [self initialize_Made_Light_4:16*widthOffset];
    [self initialize_Contact:17*widthOffset]; 
    
    //NSLog(@"View:%d,%d,%d,%d,ScrollView:%d,%d,%d,%d",self.view.frame.origin.x,self.view.frame.origin.y,self.view.frame.size.width,self.view.frame.size.height,self.mainScrollView.frame.origin.x,self.mainScrollView.frame.origin.y,self.mainScrollView.frame.size.width,self.mainScrollView.frame.size.height);

    self.mainScrollView.contentSize = CGSizeMake(self.mainScrollView.frame.size.width*viewArray.count, self.mainScrollView.frame.size.height);

    for(int i=0;i<viewArray.count;i++)
    {
        [self.mainScrollView addSubview:[viewArray objectAtIndex:i]];
    }
    CGFloat pageWidth = mainScrollView.frame.size.width;
    //int page = floor((mainScrollView.contentOffset.x - pageWidth/2)/pageWidth)+1; 
        /*if(page == 2)
        {
            currentlyInUse = decoPotAnimatingBackground;
            animeTimer = [NSTimer scheduledTimerWithTimeInterval:3.0 target:self selector:@selector(backgroundReAnimation) userInfo:nil repeats:YES];
        }*/
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}

@end
