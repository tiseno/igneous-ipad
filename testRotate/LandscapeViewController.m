//
//  LandscapeViewController.m
//  testRotate
//
//  Created by Tiseno on 11/17/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "LandscapeViewController.h"


@implementation LandscapeViewController

@synthesize scrollView;
@synthesize timer;
@synthesize clockLabel, dateLabel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        timerWidthCounter=1;
        direction=1;
    }
    return self;
}
- (void)dealloc
{
    [super dealloc];
    [scrollView release];
}
-(void)updateTimer
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init ];
    
    [formatter setDateFormat:@"hh:mm:ss"];
    [formatter setTimeStyle:NSDateFormatterLongStyle];
    clockLabel.text = [formatter stringFromDate:[NSDate date]];
    [formatter release];
}
-(void)updateDate
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init ];
    [formatter setDateFormat:@"MM d, YYYY"];
    [formatter setDateStyle:NSDateFormatterLongStyle];
    dateLabel.text = [formatter stringFromDate:[NSDate date]];
    [formatter release];
    
}

-(void)screenSaverMoves
{
    
    CGRect movesToImage = CGRectMake((timerWidthCounter*self.scrollView.frame.size.width) , 0, self.scrollView.frame.size.width, self.scrollView.frame.size.height);
    [scrollView scrollRectToVisible:movesToImage animated:YES];
    timerWidthCounter=((timerWidthCounter+direction));
    if(timerWidthCounter==11)
    {
        timerWidthCounter--;
        timerWidthCounter--;
        direction=(-1);
    }
    if(timerWidthCounter==-1)
    {
        timerWidthCounter++;
        timerWidthCounter++;
        direction=1;
    }

    
        //[NSTimer scheduledTimerWithTimeInterval:30.0 target:self selector:@selector(screenSaverMoves) userInfo:nil repeats:NO];
    
}
-(void)addImagesToArray
{
    int contentWidth=1;
    timerWidthCounter=1;
    for(;contentWidth<12;contentWidth++)
    {
        NSString *imageName = [NSString stringWithFormat:@"wallpaper_%d.png" , contentWidth];
        UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imageName]];
        imageView.frame = CGRectMake((contentWidth-1)*self.view.frame.size.width, self.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height);
        [self.scrollView addSubview:imageView];
        [imageView release];
    }
    timer = [NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(screenSaverMoves) userInfo:nil repeats:YES];
    self.scrollView.contentSize = CGSizeMake((contentWidth-1)*self.view.frame.size.width, self.view.frame.size.height);
}
- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self addImagesToArray];
    clockTimer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(updateTimer) userInfo:nil repeats:YES];
    dateTimer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(updateDate) userInfo:nil repeats:YES];
    /*self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width*imageArray.count, self.scrollView.frame.size.height);
     
     for(int i=0;i<imageArray.count;i++)
     {
     UIImageView *imageView = [imageArray objectAtIndex:i];
     [self.scrollView addSubview:imageView];
     }*/
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}

@end
