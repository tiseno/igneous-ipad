//
//  testRotateAppDelegate.h
//  testRotate
//
//  Created by Tiseno on 11/17/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class testRotateViewController;

@interface testRotateAppDelegate : NSObject <UIApplicationDelegate> {

}

@property (nonatomic, retain) IBOutlet UIWindow *window;

@property (nonatomic, retain) IBOutlet testRotateViewController *viewController;

@end
