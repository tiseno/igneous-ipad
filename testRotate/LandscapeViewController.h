//
//  LandscapeViewController.h
//  testRotate
//
//  Created by Tiseno on 11/17/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface LandscapeViewController : UIViewController {
    UIScrollView *scrollView;
    NSTimer *timer;
    int timerWidthCounter;
    int direction;
    
    UILabel *cloclLabel;
    UILabel *dateLabel;
    
    NSTimer *clockTimer;
    NSTimer *dateTimer;
    
}
@property (nonatomic, retain) IBOutlet UIScrollView *scrollView;
@property (nonatomic, retain) NSTimer *timer;

@property (nonatomic, retain) IBOutlet UILabel *clockLabel;
@property (nonatomic, retain) IBOutlet UILabel *dateLabel;

@end
