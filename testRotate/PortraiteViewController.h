//
//  PortraiteViewController.h
//  testRotate
//
//  Created by Tiseno on 11/17/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImageViewWithAnimation.h"
#define widthOffset 768


@interface PortraiteViewController : UIViewController<UIScrollViewDelegate> {
    NSMutableArray *mainUIArray;
    NSMutableArray *viewArray;
    NSMutableArray *screenSaverImageArray;
    
    UIScrollView *scrollView;
    UIScrollView *mainScrollView;
    
    UIButton *menuButton;
    
    UILabel *cloclLabel;
    UILabel *dateLabel;
    
    NSTimer *clockTimer;
    NSTimer *dateTimer;
    NSTimer *animeTimer;
    
    UIImageView *timeAndClockBackground;
    UIImageView *decoPotAnimatingBackground;
    UIImageView *artCollectionAnimatingBackground;
    UIImageView *cravedStrongAnimatingBackground;
    UIImageView *madeLightAnimatingBackground;
    UIImageView *currentlyInUse;
    
    BOOL isExpanded;
    BOOL isLightOn;
    
    int currentPage;
    int animationOffset;
}
@property (nonatomic, retain) NSMutableArray *mainUIArray;
@property (nonatomic, retain) NSMutableArray *viewArray;
@property (nonatomic, retain) NSMutableArray *screenSaverImageArray;
@property (nonatomic, retain) IBOutlet UIScrollView *mainScrollView;

@property (nonatomic, retain) IBOutlet UILabel *clockLabel;
@property (nonatomic, retain) IBOutlet UILabel *dateLabel;

@property (nonatomic, retain) IBOutlet UIButton *menuButton;

@property (nonatomic, retain) IBOutlet UIScrollView *scrollView;

@property (nonatomic, retain) IBOutlet UIImageView *timeAndClockBackground;
@property (nonatomic, retain) UIImageView *decoPotAnimatingBackground;
@property (nonatomic, retain) UIImageView *artCollectionAnimatingBackground;
@property (nonatomic, retain) UIImageView *cravedStrongAnimatingBackground;
@property (nonatomic, retain) UIImageView *madeLightAnimatingBackground;
@property (nonatomic, retain) UIImageView *currentlyInUse;

@property (nonatomic, retain) NSTimer *clockTimer;
@property (nonatomic, retain) NSTimer *dateTimer;
@property (nonatomic, retain) NSTimer *animeTimer;

-(void)addToViewArray:(UIView*) view;

-(IBAction)menuButtonTapped;
-(IBAction)buttonOneTapped;
-(IBAction)buttonTwoTapped;
-(IBAction)buttonThreeTapped;
-(IBAction)buttonFourTapped;
-(IBAction)buttonFiveTapped;
-(void)updateTimer;
-(void)updateDate;
@end
